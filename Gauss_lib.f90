﻿module Gauss_lib

    implicit none

    contains
    
    subroutine Integrate(a, b, n, f, resulty)
    real(8), intent(in) :: a, b
    integer(4), intent(in) :: n
    real(8), intent(out) :: resulty
    real(8), dimension(1:n) :: A_array, t
    integer(4) :: i
    
    interface
        function f(x)
        real(8), intent(in) :: x
        real(8) :: f
        end function
    end interface
    
    resulty = 0

    call ReadData(n, A_array, t)
    
    do i=1,n
        resulty = resulty + A_array(i)*(b-a) / 2*f( t(i)*(b-a)/2 + (a+b)/2 )
    enddo

    end subroutine Integrate
    
    function GetFileName(n) result(filename)
    integer(4), intent(in) :: n
    character(len=14) :: filename
    character(len=12) :: format_string
    
        if (n < 10) then
            format_string = "(A4, I1, A4)"
        else
            format_string = "(A4, I2, A4)"
        endif
            write (filename,format_string) "quad", n, ".dat"
    
    end function GetFileName
    
    subroutine ReadData(n, A_array, t)
    character(len=14) :: filename
    real(8), dimension(1:n), intent(out) :: A_array, t
    integer(4), intent(in) :: n 
    integer(4) :: i

    filename = GetFileName(n)
    
    open(1, file=filename)
        do i=1,n
            read(1,*) A_array(i), t(i)
        enddo
    close(1)
    
    end subroutine ReadData
    
    function f(x)
    real(8), intent(in) :: x
    real(8) :: f
    integer(4) :: i

    f=3*x**2+42

    end function f
end module Gauss_lib
    