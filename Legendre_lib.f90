﻿module Legendre_lib
    use Gauss_lib
    use BernoulliHorner
    use methods
    implicit none
    
    contains
    
    subroutine MakeQuad(n)
    integer(4), intent(in) :: n
    real(8), allocatable :: A(:), t(:), M(:,:)
    integer(4) :: i
    
    allocate(A(1:n), t(n), M(1:n+1, 0:n-1))
    
    call LegendreRoot(n, t)
     
    forall (i=0:n-1) M(1:n,i) = t**i
    M(n+1,1:n-1:2)=0
    forall (i=0:n-1:2) M(n+1,i) = 2.0/(i+1)

    call choise(M, A, n)
    call WriteData(n, A, t)
    end subroutine MakeQuad
    
    subroutine WriteData(n, A, t)
    character(len=14) :: filename
    real(8), dimension(1:n), intent(in) :: A, t
    integer(4), intent(in) :: n 
    integer(4) :: i

    filename = GetFileName(n)
    
    open(2, file=filename)
        do i=1,n
            write(2,*) A(i), t(i)
        enddo
    close(2)
    
    end subroutine WriteData
    
    
    recursive subroutine LegendreCoeff(n, A)
    integer(4), intent(in) :: n
    real(8), dimension(0:n), intent(out) :: A
    real(8), dimension(0:n-1) :: P1
    real(8), dimension(0:n-2) :: P2
    integer(4) :: i
    
    
    if (n==0) then
        A(0) = 1
    else if (n==1) then
        A(0:1) = (/1.0,0.0/)
    else
        call LegendreCoeff(n-1, P1)
        call LegendreCoeff(n-2, P2)
        
        A(0) = (2*n-1.0)/n * P1(0)
        A(1) = (2*n-1.0)/n * P1(1)
        
        forall (i=2:n-1) A(i) = (2*n-1.0)/n*P1(i)-(n-1.0)/n*P2(i-2)
        A(n) = -(n-1.0)/n*P2(n-2)
    end if

    end subroutine LegendreCoeff

    
    subroutine LegendreRoot(n, X)
    integer, intent(in) :: n
    real(8), dimension(1:n), intent(out) :: X
    real(8), dimension(0:n) :: A
    
    select case(n) !корни полинома меньше четвертой степени так не ищутся, поэтому воспользуемся калькулятором
        case(1); X=0.0
        case(2); X=(/0.5773503,-0.5773503/)
        case(3); X=(/0.7745967,-0.7745967,0.0/)
        case default
            call LegendreCoeff(n, A)
            call Bernoulli(A, X)
    end select

    end subroutine LegendreRoot
end module Legendre_lib