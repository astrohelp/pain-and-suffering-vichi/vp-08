﻿module BernoulliHorner

    implicit none
    
    contains
    
    subroutine Bernoulli(A0,X)
    real(8), dimension(0:), intent(in) :: A0
    real(8), dimension(0:(size(A0)-1)/2) :: A, B
    real(8), dimension(1:size(A0)-1), intent(out) :: X
    real(8), dimension(1:size(A0)-1) :: Y
    real(8) :: root, sqroot
    integer(4) :: n, divsCount

    n=size(A0)-1

    if (mod(n,2).ne.0) then
        X(n)=0.0
    endif
    
    !у полинома Лежандра корни парные
    !введем замену t=x^2, тогда останется найти корни многочлена степени n/2

    A(0:n/2) = A0(0:n:2)
    

    do divsCount=0, n/2 - 3
        call RANDOM_NUMBER(Y)
        call FindMaxRoot(A(0:n/2-divsCount), Y(1:n/2-divsCount), root)
        
        X(2*divsCount+1) = sqrt(root)
        X(2*divsCount+2) = -sqrt(root)
        
        call Horner(A(0:n/2-divsCount), B(0:n/2-divsCount), root)
        A(0:n/2-divsCount-1) = B(0:n/2-divsCount-1)
    enddo

    sqroot = sqrt(A(1)**2 - 4*A(0)*A(2))
    X(2*divsCount+1) = sqrt( (-A(1) + sqroot)/(2*A(0)) )
    X(2*divsCount+2) = -X(2*divsCount+1)
    X(2*divsCount+3) = sqrt( (-A(1) - sqroot)/(2*A(0)) )
    X(2*divsCount+4) = -X(2*divsCount+3)

    end subroutine Bernoulli
    
    subroutine Horner(A, B, root)
    real(8), dimension(0:), intent(in) :: A
    real(8), intent(in) :: root
    real(8), dimension(0:size(A)-1), intent(out) :: B
    integer :: n, i

    n = size(A) - 1
    B(0) = A(0)
    
    do i = 1,n
        B(i) = root*B(i-1) + A(i)
    enddo

    end subroutine Horner
    
    subroutine FindMaxRoot(A, Y, root)
    real(8), dimension(0:), intent(in) :: A
    real(8) :: root
    real(8), dimension(1:size(A)) :: Y
    integer(4) :: i, n
    real(8) :: eps=0.000000001

    n=size(A)-1
    
    do while (abs( Y(n)/Y(n-1) - Y(n-1)/Y(n-2) ) > eps)
        Y(n+1) = sum( (A(1:n)/(-A(0)))*Y(n:1:-1) )
        forall (i=1:n-1) Y(i)=Y(i+1)
        Y(n)=Y(n+1)
    enddo
    
    root=Y(n)/Y(n-1)

    end subroutine FindMaxRoot
end module BernoulliHorner