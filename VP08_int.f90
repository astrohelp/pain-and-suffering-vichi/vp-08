﻿program GaussIntegrate
    
    use Gauss_lib
    implicit none
    
    real(8), parameter :: a=1.0, b=6.0
    integer(4), parameter :: n=9
    real(8) :: resulty !result является ключевым словом и компилятор кидается ошибкой. Поэтому будет результатик.
    
    interface
        function ff(x)
        real(8), intent(in) :: x
        real(8) :: ff
        end function
    end interface
    
    call Integrate(a, b, n, f, resulty)

    write(*,*) resulty
   
end program GaussIntegrate